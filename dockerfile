FROM mcr.microsoft.com/dotnet/core/sdk:2.1
ENV HOME=/home/vexter-api-audit
WORKDIR $HOME
COPY . $HOME
RUN dotnet restore
RUN dotnet publish ./
ENTRYPOINT ["dotnet", "bin/Debug/netcoreapp2.1/vexter_api_audit_console.dll"]