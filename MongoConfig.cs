using System;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Core.Clusters;

namespace vexter_api_audit_console
{
    class MongoConfig
    {
        public IMongoCollection<MongoDB.Bson.BsonDocument> returnCollection()
        {
            MongoClient client = new MongoClient("mongodb://mongo-vexter-api-audit:27017");
            if (CheckConnection(client))
            {
                IMongoDatabase database = client.GetDatabase("Audit");
                return database.GetCollection<BsonDocument>("AuditLogs");
            }
            return null;
        }

        private bool CheckConnection(MongoClient client)
        {
            try
            {
                client.ListDatabases();
            }
            catch (Exception)
            {
            }

            return client.Cluster.Description.State == ClusterState.Connected;
        }
    }
}