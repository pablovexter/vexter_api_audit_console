using System;

namespace vexter_api_audit_console.Models
{
    public class AuditModel
    {
        public string userId { get; set; }
        public string ip { get; set; }
        public string additionalInfo{ get; set; }
        public DateTime? createdAt { get; set; }
    }
}