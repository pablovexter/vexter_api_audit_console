using System;

namespace vexter_api_audit_console.Models
{
    public class MongoDBConfig
    {
        public string Client { get; set; }
        public string Database { get; set; }
    }
}