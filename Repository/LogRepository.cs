using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using vexter_api_audit_console.Models;

namespace vexter_api_audit_console.Repository
{
    public class LogRepository
    {
        private readonly IServiceCollection _services;
        private readonly IConfiguration _configuration;
        private readonly MongoDBConfig _mongoDBConfig;
        private readonly IMongoCollection<dynamic> _collectionMongo;


        public LogRepository()
        {
            _mongoDBConfig.Client = "mongodb://localhost:27017/";
            _mongoDBConfig.Database = "authentication";
            IMongoClient client = new MongoClient(_mongoDBConfig.Client);
            IMongoDatabase database = client.GetDatabase(_mongoDBConfig.Database);
            _collectionMongo = database.GetCollection<dynamic>("AuditModel");
        }


        public async Task Insert(
            object Audit)
        {
            Console.WriteLine(_mongoDBConfig.Client);
            try
            {
                _collectionMongo.InsertOne(Audit);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}