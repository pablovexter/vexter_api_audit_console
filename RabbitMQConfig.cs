using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace vexter_api_audit_console
{
    class RabbitMQConfig
    {      
        private static IConfiguration _configuration;
        public IModel returnChannel(string hostName)
        { 
            ConnectionFactory factory = new ConnectionFactory()
            {
                HostName = hostName,
            };

            IConnection connection = factory.CreateConnection();
            return connection.CreateModel();
        }       
    }
}