﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace vexter_api_audit_console
{
    class Program
    {
        private static IConfiguration _configuration;
        private static AutoResetEvent waitHandle = new AutoResetEvent(false);
        static void Main(string[] args)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json");
            _configuration = builder.Build();

            RabbitMQConfigurations rabbitMQConfigurations = new RabbitMQConfigurations();

            new ConfigureFromConfigurationOptions<RabbitMQConfigurations>(
                _configuration.GetSection("RabbitMq"))
                .Configure(rabbitMQConfigurations);

            RabbitMQConfig rabbitMQConfig = new RabbitMQConfig();

            using (IModel channel = rabbitMQConfig.returnChannel(rabbitMQConfigurations.HostName))
            {
                channel.QueueDeclare(queue: "audit",
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
                Console.WriteLine("**** CONEXÃO COM FILA");
                consumer.Received += (model, ea) =>
            {
                byte[] body = ea.Body;
                string message = Encoding.UTF8.GetString(body);
                BsonDocument document = BsonDocument.Parse(message);
                MongoConfig _mongoConfig = new MongoConfig();
                try
                {
                    _mongoConfig.returnCollection().InsertOneAsync(document);
                }
                catch (Exception)
                {
                    byte[] messageBuffer = Encoding.Default.GetBytes(message);
                    channel.BasicPublish("direct.exchange", "", null, messageBuffer);
                }
            };

                channel.BasicConsume(queue: "audit",
                            autoAck: true,
                            consumer: consumer);

                Console.CancelKeyPress += (o, e) =>
            {
                Console.WriteLine("Saindo...");
                waitHandle.Set();
            };
                waitHandle.WaitOne();
            }
        }
    }
}

